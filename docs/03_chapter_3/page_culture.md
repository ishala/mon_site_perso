---
author: Inès Shala
title: 🎥 Culture
---

# 🎥 Culture

## Bollywood

Let yourself immerse in the enchanting world of Bollywood movies. 

??? abstract "Om Shanti Om by Farah Khan (2007)"
    ![Om Shanti Om](images/Om_Shanti_Om.jpg){ width=20% }

    * **Genre**: Action, Comedy, Drama
    * **Synopsis**: In the 1970s, Om, an aspiring actor, is murdered, but is immediately reincarnated into the present day. He attempts to discover the mystery of his demise and find Shanti, the love of his previous life.
    * **Stars**: Shah Rukh Khan, Deepika Padukone, Arjun Rampal
    * [Click here](https://www.imdb.com/title/tt1024943/) for more information

??? abstract "3 Idiots by Rajkumar Hirani (2009)"
    ![3 Idiots](images/3_idiots_poster.jpg){ width=20% }

    * **Genre**: Comedy, Drama
    * **Synopsis**: Two friends are searching for their long lost companion. They revisit their college days and recall the memories of their friend who inspired them to think differently, even as the rest of the world called them "idiots".
    * **Stars**: Aamir Khan, Madhavan, Mona Singh
    * [Click here](https://www.imdb.com/title/tt1187043/?ref_=fn_al_tt_1) for more information 

??? abstract "The Lunchbox by Ritesh Batra (2013)"
    ![The Lunchbox](images/Lunchbox.jpg){ width=20% }

    * **Genre**: Drama, Romance
    * **Synopsis**: A mistaken delivery in Mumbai's famously efficient lunchbox delivery system connects a young housewife to an older man in the dusk of his life as they build a fantasy world together through notes in the lunchbox.
    * **Stars**: Irrfan Khan, Nimrat Kaur, Nawazuddin Siddiqui
    * [Click here](https://www.imdb.com/title/tt2350496/?ref_=fn_al_tt_1) for more information

??? abstract "Queen by Vikas Bahl (2013)"
    ![Queen](images/queen.jpg){ width=20% }

    * **Genre**: Adventure, Comedy, Drama
    * **Synopsis**: A Delhi girl from a traditional family sets out on a solo honeymoon after her marriage gets cancelled.
    * **Stars**: Kangana Ranaut, Rajkummar Rao, Lisa Haydon
    * [Click here](https://www.imdb.com/title/tt3322420/?ref_=fn_al_tt_2) for more information
    
??? abstract "Dangal by Nitesh Tiwari (2016)"
    ![Dangal](images/Dangal.jpg){ width=20% }

    * **Genre**: Action, Biography, Drama
    * **Synopsis**: Former wrestler Mahavir Singh Phogat and his two wrestler daughters struggle towards glory at the Commonwealth Games in the face of societal oppression.
    * **Stars**: Aamir Khan, Sakshi Tanwar, Fatima Sana Shaikh
    * [Click here](https://www.imdb.com/title/tt5074352/?ref_=tt_mv_close) for more information