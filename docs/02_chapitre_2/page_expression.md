---
author: Inès Shala
title: 👋🏽 Expressions
---

# 👋🏽 Expressions

## Greetings

??? abstract "Hello/Goodbye"
    * नमस्ते : *Namaste*
    * Friendly greeting that can be used anytime during the day

??? abstract "Hello/Goodbye: formal"
    * नमस्कार : *Namaskar*
    * More formal that Namaste

??? abstract "See you"
    * फिर मिलेंगे : *Phir milenge*
    * *Phir* = again
    * *milenge* = we will meet

??? abstract "Goodbye"
    * अलविदा: *Alvida*

## Introduction
??? abstract "My name is ..."
    * मेरा नाम ... है। : *Meraa naam (your name) hai.*

??? abstract "Nice to meet you."
    * आप से मिलकर खुशी हुई। : *Aap se milakar khushee hui.*

## Express gratitude
??? abstract "Thank you"
    * शुक्रिया: *Shukriya*
    * You can emphasis by adding बहुत : *bahut* before, it means "thank you very much"

??? abstract "Thank you: formal"
    * धन्यवाद: *Dhanyavaad*
    * You can emphasis by adding बहुत : *bahut* before, it means "thank you very much"

??? abstract "You're welcome"
    * स्वागत है: *svaagat hai*

??? abstract "No problem"
    * कोई बात नहीं: *koii baat nahee*
    
    