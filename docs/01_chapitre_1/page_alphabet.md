---
author: Inès Shala
title: 🕉️ Alphabet and Numbers
---

# 🕉️ Alphabet and Numbers

## Independent Vowels
??? abstract "Independant Vowels"
    | Letter | Transcription | Pronunciation Example |
    |------------|---------------|-----------------------|
    | **अ**      | a             | as in apple           |
    | **आ**      | aa            | as in hat             |
    | **इ**      | i             | as in history         |
    | **ई**      | ii            | as in Wii             |
    | **उ**      | u             | as in ultimate        |
    | **ऊ**      | oo            | as in noon            |
    | **ऋ**      | ri            | as in ring            |
    | **ए**      | e             | as in energy          |
    | **ऐ**      | ai            | as in mate            |
    | **ओ**      | o             | as in home            |
    | **औ**      | au            | as in mouse           |
    | **अं**     | un            | as in umbrella        |
    | **अः**     | uh            | as in maharaja        |

## Dependent Vowels
??? abstract "Depedent Vowels"
    | Letter      | ा  | ि | ी  | ु | ू  | ृ | ॄ  | ॅ |
    |-----------------|----|---|----|---|----|---|----|---|
    | Transcription | aa | i | ii | u | uu | r | rr | e |


    | Letter        | ॆ | े | ै  | ॉ | ॊ | ो | ौ  |   
    |-----------------|---|---|----|---|---|---|----|
    | Transcription | e | e | ai | o | o | o | au | 

## Consonants 
??? abstract "Consonants"
    | **Letter** | Transcription | Pronunciation Example |
    |------------|---------------|-----------------------|
    | **क**      | k             | as in Kashmir         |
    | **ख**      | kha           | as in Khan            |
    | **ग**      | g             | as in garden          |
    | **घ**      | gha           | as in sugar           |
    | **ङ**      | nga           | as in knock           |
    | **च**      | ca            | as in church          |
    | **छ**      | cha           | as in nature          |
    | **ज**      | ja            | as in Jasmine         |
    | **झ**      | jha           | as in surgery         |
    | **ञ**      | nya           | as in Niagara         |
    | **ट**      | ta            | as in talk            |
    | **ठ**      | thh           | as in tongue          |
    | **ड**      | dh            | as in dog             |
    | **ण**      | n             | as in Nigeria         |
    | **त**      | t             | as in thumb           |
    | **थ**      | tha           | as in Thailand        |
    | **द**      | d             | as in buddha          |
    | **ध**      | dha           | as in Radha           |
    | **न**      | na            | as in novel           |
    | **प**       | p              | as in public          |
    | **फ**       | fa             | as in sofa            |
    | **ब**       | b              | as in bath            |
    | **भ**       | bha            | as in bucket          |
    | **म**       | ma             | as in mother          |
    | **य**       | y              | as in asia            |
    | **र**       | r              | as in basket          |
    | **ल**       | la             | as in violet          |
    | **व**       | v              | 	as in vast           |
    | **श**       | sa             | as in saw             |
    | **ष**       | sha            | as in ocean           |
    | **स**       | ssa            | as in reason          |
    | **ह**       | ha             | as in hard            |
    | **क्ष**     | ksh            | as in kshatriya	      |
    | **त्र**     | tra            | as in prong           |
    | **ज्ञ**     | gya            | as in yajna           |




## Counting in Hindi
??? abstract "Numbers"
    | **Number** | **Number in Hindi** | **Transcription** |
    |------------|---------------------|-------------------|
    | 0          | ०                   | Shunya            |
    | 1          | १                   | Ek                |
    | 2          | २                   | Do                |
    | 3          | ३                   | Theen             |
    | 4          | ४                   | Chaar             |
    | 5          | ५                   | Paanch            |
    | 6          | ६                   | Chay              |
    | 7          | ७                   | Saath             |
    | 8          | ८                   | Aat               |
    | 9          | ९                   | No                |
    | 10         | १०                  | Dus               |




!!! tip "Challenge"

    Learn to say your phone number in Hindi !